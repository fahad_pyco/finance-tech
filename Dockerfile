FROM openjdk:11.0.7-jdk

ADD target/finance-tech-0.0.1-SNAPSHOT.jar /opt/app.jar

EXPOSE 8081

ENTRYPOINT exec java -jar /opt/app.jar