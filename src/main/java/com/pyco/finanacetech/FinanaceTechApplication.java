package com.pyco.finanacetech;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FinanaceTechApplication {

    //main method of finance tech application
	public static void main(String[] args) {
		SpringApplication.run(FinanaceTechApplication.class, args);
	}

}
